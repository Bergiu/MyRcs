# Path to your oh-my-zsh installation.
# spacemacs tramp mode fix
[[ $TERM == "dumb" ]] && unsetopt zle && PS1='$ ' && return

export ZSH=~/.oh-my-zsh
export TERM=xterm-256color
# export TERM=tmux-256color

# Themes
# See https://github.com/robbyrussell/oh-my-zsh/wiki/Themes
ZSH_THEME="robbyrussell"
ZSH_THEME="emojirussell"

# disables the annoying beep
setopt NO_BEEP

# Uncomment the following line to use case-sensitive completion.
# CASE_SENSITIVE="true"

# Uncomment the following line to use hyphen-insensitive completion. Case
# sensitive completion must be off. _ and - will be interchangeable.
# HYPHEN_INSENSITIVE="true"

# Uncomment the following line to disable bi-weekly auto-update checks.
# DISABLE_AUTO_UPDATE="true"

# Uncomment the following line to change how often to auto-update (in days).
# export UPDATE_ZSH_DAYS=13

# Uncomment the following line to disable auto-setting terminal title.
# DISABLE_AUTO_TITLE="true"

# Uncomment the following line to enable command auto-correction.
# ENABLE_CORRECTION="true"

# Uncomment the following line to display red dots whilst waiting for completion.
# COMPLETION_WAITING_DOTS="true"

# Uncomment the following line if you want to disable marking untracked files
# under VCS as dirty. This makes repository status check for large repositories
# much, much faster.
# DISABLE_UNTRACKED_FILES_DIRTY="true"

# Uncomment the following line if you want to change the command execution time
# stamp shown in the history command output.
# The optional three formats: "mm/dd/yyyy"|"dd.mm.yyyy"|"yyyy-mm-dd"
# HIST_STAMPS="mm/dd/yyyy"


# VIM key bindings
bindkey -v
# to disable ctrl+s hanging my shell on saving
stty -ixon

# Plugins
plugins=(git vi-mode zsh-syntax-highlighting wd zsh-autosuggestions)
# maybe: wd tmux

# load oh my zsh
source $ZSH/oh-my-zsh.sh

# load my environment vars
source ~/.environ
# load my aliases
source ~/.aliases
if hash kubectl 2>/dev/null; then
    source <(kubectl completion zsh)
fi

[ -f ~/.fzf.zsh ] && source ~/.fzf.zsh

