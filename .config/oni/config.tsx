import * as React from "/opt/oni/resources/app/node_modules/react"
import * as Oni from "/opt/oni/resources/app/node_modules/oni-api"
declare var require: any

export const activate = (oni: Oni.Plugin.Api) => {
  console.log("config activated")

  // Input
  //
  // Add input bindings here:
  //
  oni.input.bind("<c-enter>", () => console.log("Control+Enter was pressed"))
  oni.input.bind("<f8>", "markdown.togglePreview")
  // oni.input.bind("<f7>", () =>
  //   window.location.replace("http://www.w3schools.com")
  // )

  //
  // Or remove the default bindings here by uncommenting the below line:
  //
  // oni.input.unbind("<c-p>")
}

export const deactivate = (oni: Oni.Plugin.Api) => {
  console.log("config deactivated")
}

export const configuration = {
  //add custom config here, such as

  "ui.colorscheme": "gruvbox",

  //"oni.useDefaultConfig": true,
  //"oni.bookmarks": ["~/Documents"],
  //"oni.loadInitVim": false,
  //"editor.fontSize": "12px",
  //"editor.fontFamily": "Monaco",

  // UI customizations
  "ui.animations.enabled": true,
  "ui.fontSmoothing": "auto",
  "tabs.mode": "tabs",
  "achievements.enabled": false,
  "autoUpdate.enabled": true,
  "sidebar.default.open": false,
  "sidebar.enabled": false,
  "experimental.markdownPreview.enabled": true,
}

/**
 * My notes:
 *
 * Cool features:
 * - KeyDisplayer: Show full modifier key names (#2340 - thanks @Speculative!)
 *  - <c-s-p>input: show key presses
 *  - <c-s-p>input: hide key presses
 **/
