""""""""""""""""
" Vim Settings "
""""""""""""""""

"""""""""""
" FILETYPE
"""""""""""
au BufReadPost *.cls set filetype=tex

"""""""""""
" SPELLING
"""""""""""
set spell spelllang=en,de
set nospell

"""""""""""""
" APPEARANCE
"""""""""""""
set number              " Line numbers
set relativenumber      " Relative line numbers
set list                " Display all characters
set listchars=eol:¬,tab:•\ ,trail:•,extends:»,precedes:«
if has("patch-7.4.710")
    set listchars+=space:␣
endif

"""""""""
" RANDOM
"""""""""
syntax on               " Switch syntax highlighting on
set exrc                " load locale vim file
set encoding=utf8
set backspace=indent,eol,start      " Make backspace behave in a sane manner.
set hidden              " allow me to have buffers with unsaved changes
set autoread            " when a file has changed on disk, just load it. Don't ask
set path+=**            " find every file recursively from current position
set tags=./tags;/       " ctags file

""""""""""""""
" INDENTATION
""""""""""""""
set expandtab           " Use spaces, alias is "noet"
set autoindent          " Match indents on new lines
set shiftwidth=4        " Use 4 spaces on intending
set tabstop=4           " A tab is 4 chars long
set softtabstop=4
autocmd FileType make,go setlocal noet
autocmd FileType js,ts,vue,nim setlocal tabstop=2
autocmd FileType js,ts,vue,nim setlocal shiftwidth=2
autocmd FileType js,ts,vue,nim setlocal softtabstop=2

"""""""""
" SEARCH
"""""""""
set ignorecase          " case insensitive
set smartcase           " case sensitive, if search contains uppercase letters
set hlsearch            " highlight matches
set incsearch           " live incremental searching
set showmatch           " live match hightlighting

""""""""""""
" VIM FILES
""""""""""""
set nobackup            " We have vcs (version control system), we don't need backups
set nowritebackup       " Same here
set noswapfile          " Don't make the swap files
set undofile            " use an undo file
set undodir=$HOME/.nvim/vimundo/        " this is the undo file

""""""""""""""""""""
" TERMINAL BEHAVIOUR
""""""""""""""""""""
set noerrorbells        " Kein Piepsen
set visualbell
set ttyfast             " schnelleres terminal

""""""""""""""""""
" MOUSE BEHAVIOUR
""""""""""""""""""
set mouse+=a
if &term =~ '^screen'
    " tmux knows the extended mouse mode
    set ttymouse=xterm2
endif

""""""""""""""""""""
" SPECIAL BEHAVIOUR
""""""""""""""""""""
" AUTOMATICALLY JUMP TO LAST POSITION
if has("autocmd")
    au BufReadPost * if line("'\"") > 0 && line("'\"") <= line("$")
                \| exe "normal! g'\"" | endif
endif

""""""""""""""""""""""""""""
" needed to work with venv "
""""""""""""""""""""""""""""
let g:python3_host_prog = '/usr/bin/python3'


noremap <Up> <Nop>
noremap <Down> <Nop>
noremap <Left> <Nop>
noremap <Right> <Nop>
