""""""""""""""""
" Color scheme "
""""""""""""""""
set t_Co=256
set background=light
" colorscheme gruvbox
colorscheme PaperColor

" custom colors
highlight SpellBad cterm=underline
" highlight SyntasticError ctermfg=White
" highlight SyntasticError ctermbg=DarkRed
" highlight SyntasticError cterm=bold
" highlight Search ctermbg=Yellow
" highlight Search cterm=bold
" highlight MatchParen cterm=none ctermbg=green ctermfg=blue
