source ~/.config/nvim/01_preconfigure.vim
source ~/.config/nvim/02_plugin_install.vim
source ~/.config/nvim/03_settings.vim
source ~/.config/nvim/04_plugin_config.vim
source ~/.config/nvim/05_keybindings.vim
source ~/.config/nvim/06_colorscheme.vim
