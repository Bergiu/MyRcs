""""""""""""""""""""""""""""""""""""""""""""""""""""
"                    APPEARANCE                    "
""""""""""""""""""""""""""""""""""""""""""""""""""""

""""""""""
" Airline
""""""""""
let g:airline#extensions#tabline#enabled = 1
let g:airline#extensions#tabline#left_sep = ' '
let g:airline#extensions#tabline#left_alt_sep = '|'
let g:airline_solarized_bg='light'
let g:airline_theme='sol'
let g:airline_powerline_fonts = 1 " ASCII oder UNICODE
set laststatus=2


"""""""""""""""""""""""""""""""""""""""""""""""""""
"                    BEHAVIOUR                    "
"""""""""""""""""""""""""""""""""""""""""""""""""""


"""""""""""""""""""""""""""""""""""""""""""""""""
"                    UTILITY                    "
"""""""""""""""""""""""""""""""""""""""""""""""""
"""""""""""
" Thesaurus
"""""""""""
let g:tq_enabled_backends=["cilin_txt",
            \"openthesaurus_de",
            \"yarn_synsets",
            \"openoffice_en",
            \"mthesaur_txt",
            \"datamuse_com",]
let g:tq_language=['en', 'de']

""""""""""""""""""
" Mardown Preview
""""""""""""""""""
" let vim_markdown_preview_toggle=2
" let vim_markdown_preview_pandoc=1
let vim_markdown_preview_github=1
" let vim_markdown_preview_browser='Firefox'
let vim_markdown_preview_use_xdg_open=1
autocmd BufRead,BufNewFile *.rmd set filetype=rmarkdown

"""""""""
" Pandoc
"""""""""
" disable folding
let g:pandoc#folding#level=10


""""""""""""""""""""""""""""""""""""""""""""""""""""""
"                    FILE MANAGER                    "
""""""""""""""""""""""""""""""""""""""""""""""""""""""

"""""""""""""""""""""""""""
" CtrlP Fuzzy File Manager
"""""""""""""""""""""""""""
" Ignore files
set wildignore+=*/tmp/*,*.so,*.swp,*.zip,*/node_modules/*
let g:ctrlp_custom_ignore = {
            \ 'dir':  '\v[\/]\.(git|hg|svn)$',
            \ 'file': '\v\.(exe|so|dll)$',
            \ 'link': 'SOME_BAD_SYMBOLIC_LINKS',
            \ }
" Local Working Directory
let g:ctrlp_working_path_mode = 'ra'
let g:ctrlp_cmd = 'CtrlPLastMode'
let g:ctrlp_extensions = ['buffertag', 'tag', 'line', 'dir']

"""""""""""""""
" NerdTree Git
"""""""""""""""
let g:NERDTreeGitStatusIndicatorMapCustom = {
                \ 'Modified'  :'✹',
                \ 'Staged'    :'✚',
                \ 'Untracked' :'✭',
                \ 'Renamed'   :'➜',
                \ 'Unmerged'  :'═',
                \ 'Deleted'   :'✖',
                \ 'Dirty'     :'✗',
                \ 'Ignored'   :'☒',
                \ 'Clean'     :'✔︎',
                \ 'Unknown'   :'?',
                \ }


""""""""""""""""""""""""""""""""""""""""""""""""
"                    SYNTAX                    "
""""""""""""""""""""""""""""""""""""""""""""""""

"""""""""""""""""""""""""
" DokuWiki Syntax Plugin
"""""""""""""""""""""""""
autocmd BufRead,BufNewFile *.{wiki} setlocal filetype=dokuwiki


""""""""""""""""""""""""""""""""""""""""""""""""""
"                    SNIPPETS                    "
""""""""""""""""""""""""""""""""""""""""""""""""""

"""""""""""""
" Neosnippet
"""""""""""""
" Enable snipMate compatibility feature.
let g:neosnippet#enable_snipmate_compatibility = 1
" Tell Neosnippet about the other snippets
let g:neosnippet#snippets_directory='~/.config/nvim/snippets'


""""""""""""""""""""""""""""""""""""""""""""""""""
"                    DEBUGGER                    "
""""""""""""""""""""""""""""""""""""""""""""""""""
" Shortcuts: https://puremourning.github.io/vimspector-web/
let g:vimspector_enable_mappings = 'HUMAN'
" mnemonic 'di' = 'debug inspect' (pick your own, if you prefer!)

nmap <F17> :call vimspector#Launch()<cr>
" for normal mode - the word under the cursor
nmap <Leader>di <Plug>VimspectorBalloonEval
" for visual mode, the visually selected text
xmap <Leader>di <Plug>VimspectorBalloonEval


"""""""""""""""""""""""""""""""""""""""""""""""""
"                    LINTING                    "
"""""""""""""""""""""""""""""""""""""""""""""""""

""""""
" ALE
""""""
let b:ale_fixers = {
            \ '*': ['remove_trailing_lines', 'trim_whitespace'],
            \ 'typescript': ['tslint'],
            \ 'java': [],
            \ 'sh': ['shfmt'],
            \ 'python': ['isort', 'yapf'],
            \ 'rust': ['rustfmt']
            \ }

let b:ale_linters = {
            \ 'java': [],
            \ 'cpp': ['ccls', 'clangcheck', 'clangtidy', 'clazy', 'cppcheck', 'cquery', 'flawfinder'],
            \ 'php': [],
            \ 'python': ["vulture"],
            \ 'rust': ['cargo'],
            \ }

let g:ale_linters = {
    \ 'cs': ['OmniSharp'],
    \}

let g:ale_linters_ignore = {
    \ 'nim': ['nimlsp'],
    \}

let g:ale_rust_cargo_use_clippy = executable('cargo-clippy')
let g:ale_python_vulture_options = "--min-confidence 100"

""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"                    COMPLETION MANAGER                    "
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

"""""""""""
" DEOPLETE
"""""""""""
let g:deoplete#enable_at_startup = 1
set completeopt-=preview  " remove preview window at the top
" autocmd CompleteDone * silent! pclose!  " close preview windows after completion
call deoplete#custom#option('sources', {
    \ 'cs': ['omnisharp'],
    \ })


"""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"                    LANGUAGE SERVER                    "
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""

" OmniSharp
let g:OmniSharp_server_use_mono = 1
let g:OmniSharp_server_stdio = 1
let g:OmniSharp_highlight_types = 2
let g:OmniSharp_selector_ui = 'fzf'    " Use fzf.vim

" Installation:
"   pacman -S mono
"   yay packages:
"     - aspnet
"     - dotnet-runtime
"     - dotnet-sdk
"     - mono-nightly
"     - msbuild
"     - msbuild-sdkresolver
" Next Steps:
" https://www.reddit.com/r/archlinux/comments/cx64r5/the_state_of_net_core_on_arch/
"   mv /opt/dotnet /usr/share/dotnet
"   rm /usr/bin/dotnet
"   ln -s /usr/share/dotnet/dotnet /usr/bin/dotnet
" Project Setup:
"   1. Create solution file: dotnet new sln
"   2. Add csproj to sln: dotnet sln add project.csproj
" Finally:
"   rm -Rf ~/.nuget/NuGet
"   ln -sfT ~/.config/NuGet ~/.nuget/NuGet

"""""""""""""""""
" LanguageClient
"""""""""""""""""
let g:LanguageClient_serverCommands = {
            \ 'cpp': ['clangd'],
            \ 'c': ['clangd'],
            \ 'java': ['jdtls'],
            \ 'python': ['pyls'],
            \ 'rust': ['rustup', 'run', 'stable', 'rls'],
            \ 'dart': ['dart', "%", "--lsp"],
            \ 'fortran': ['fortls'],
            \ 'typescript': ['typescript-language-server', '--stdio'],
            \ 'javascript': ['/usr/lib/node_modules/javascript-typescript-langserver/lib/language-server-stdio.js'],
            \ 'javascript.jsx': ['/usr/lib/node_modules/javascript-typescript-langserver/lib/language-server-stdio.js'],
            \ }
" Requirements:
" Python: palantir
"   - pip install 'python-language-server[all]'
"   - https://github.com/palantir/python-language-server
" CPP: clangd
" Java: eclipse jdt ls
"   - https://github.com/eclipse/eclipse.jdt.ls
" Typescript: (maybe its outdated and languege-server-strio is the alternative)
"   - npm install -g typescript-language-server
" Javascript:
"   - npm -g install javascript-typescript-langserver
" Fortran:
"   - pip install fortran-language-server

let g:LanguageClient_diagnosticsEnable = 1
let g:LanguageClient_loggingLevel = 'DEBUG'
let g:LanguageClient_loggingFile = '/tmp/languageClient'
let g:LanguageClient_serverStderr = '/tmp/languageServer.stderr'
let g:LanguageClient_rootMarkers = ['*.csproj', '.root', 'project.*', 'compile_commands.json', '.git']
let g:LanguageClient_trace = 'verbose'
let g:LanguageClient_useVirtualText = "No"
set signcolumn=yes


""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"                    COMPLETION MANAGER ADDITIONS                    "
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""


"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"                    LANGUAGE SERVER ADDITIONS                    "
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""


""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"                    OTHER LANGUAGE ADDITIONS                    "
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

""""""""""""""
" NIM COMMENTS
""""""""""""""
autocmd FileType nim setlocal commentstring=#\ %s

""""""""""""""""""
" PHP refactoring
""""""""""""""""""
let g:vim_php_refactoring_default_property_visibility = 'private'
let g:vim_php_refactoring_default_method_visibility = 'private'
let g:vim_php_refactoring_auto_validate_visibility = 1
let g:vim_php_refactoring_phpdoc = "pdv#DocumentCurrentLine"
let g:vim_php_refactoring_use_default_mapping = 0

