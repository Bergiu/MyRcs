call plug#begin('~/.nvim/plugged')

" TODO
"""""""
" Plug 'easymotion/vim-easymotion'
" Plug 'joonty/vim-sauce'
" Plug 'bling/vim-bufferline'
" https://github.com/tpope/vim-unimpaired

""""""""""""""""""""""""""""""""""""""""""""""""""""
"                    APPEARANCE                    "
""""""""""""""""""""""""""""""""""""""""""""""""""""
" Colorschemes
Plug 'flazz/vim-colorschemes'
Plug 'NLKNguyen/papercolor-theme'
" Airline
Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'

"""""""""""""""""""""""""""""""""""""""""""""""""""
"                    BEHAVIOUR                    "
"""""""""""""""""""""""""""""""""""""""""""""""""""
" Tmux navigator
Plug 'christoomey/vim-tmux-navigator'
" Tmux focus events (improves fugitive)
Plug 'tmux-plugins/vim-tmux-focus-events'

"""""""""""""""""""""""""""""""""""""""""""""""""
"                    UTILITY                    "
"""""""""""""""""""""""""""""""""""""""""""""""""
" Git
Plug 'tpope/vim-fugitive'
" Toggling comments
Plug 'tpope/vim-commentary'
" Surround
Plug 'tpope/vim-surround'
" Repeat
Plug 'tpope/vim-repeat'
" Easy html
Plug 'mattn/emmet-vim'
" Auto close pairs
Plug 'Raimondi/delimitMate'
" Markdown preview
" XXX (dependencies)[https://github.com/JamshedVesuna/vim-markdown-preview#requirements]
Plug 'JamshedVesuna/vim-markdown-preview'
Plug 'vim-pandoc/vim-pandoc'
Plug 'vim-pandoc/vim-pandoc-syntax'
Plug 'vim-pandoc/vim-rmarkdown'
" thesaurus
Plug 'ron89/thesaurus_query.vim'
Plug 'junegunn/fzf', { 'dir': '~/.fzf', 'do': './install --all' }
Plug 'junegunn/fzf.vim'

""""""""""""""""""""""""""""""""""""""""""""""""""""""
"                    FILE MANAGER                    "
""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Fuzzy File manager
Plug 'kien/ctrlp.vim'
" NerdTree
Plug 'scrooloose/nerdtree'
" NerdTree Git support
Plug 'Xuyuanp/nerdtree-git-plugin'

""""""""""""""""""""""""""""""""""""""""""""""""
"                    SYNTAX                    "
""""""""""""""""""""""""""""""""""""""""""""""""
Plug 'nblock/vim-dokuwiki'
Plug 'isRuslan/vim-es6'
Plug 'sheerun/vim-polyglot'

""""""""""""""""""""""""""""""""""""""""""""""""""
"                    SNIPPETS                    "
""""""""""""""""""""""""""""""""""""""""""""""""""
Plug 'Shougo/neosnippet.vim'
Plug 'Shougo/neosnippet-snippets'
Plug 'honza/vim-snippets'

""""""""""""""""""""""""""""""""""""""""""""""""""
"                    DEBUGGER                    "
""""""""""""""""""""""""""""""""""""""""""""""""""
" Plug 'vim-vdebug/vdebug'
" Plug 'huawenyu/neogdb.vim'
Plug 'puremourning/vimspector'

"""""""""""""""""""""""""""""""""""""""""""""""""
"                    LINTING                    "
"""""""""""""""""""""""""""""""""""""""""""""""""
Plug 'w0rp/ale'

""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"                    COMPLETION MANAGER                    "
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
Plug 'Shougo/deoplete.nvim', { 'do': ':UpdateRemotePlugins' }
" XXX More sources: https://github.com/Shougo/deoplete.nvim/wiki/Completion-Sources

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"                    LANGUAGE SERVER                    "
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""
Plug 'autozimu/LanguageClient-neovim', {
    \ 'branch': 'next',
    \ 'do': 'bash install.sh',
    \ }

""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"                    COMPLETION MANAGER ADDITIONS                    "
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" PHP Refactoring and Inspection (Projects must use composer and git)
" Plug 'phpactor/phpactor' ,  {'do': 'composer install'}
" Deoplete integration
" Plug 'kristijanhusak/deoplete-phpactor'

""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"                    OTHER LANGUAGE ADDITIONS                    "
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" PHP refactoring
Plug 'adoy/vim-php-refactoring-toolbox', {'for': 'php'}
Plug 'OmniSharp/omnisharp-vim'
Plug 'czheo/mojo.vim'

""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"                    KÜNSTLICHE INTELLIGENZ                      "
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
Plug 'github/copilot.vim'

call plug#end()
filetype plugin indent on
