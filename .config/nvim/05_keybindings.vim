"""""""""""""""""""""""""""""""""""""""""""""""""
"                    GENERAL                    "
"""""""""""""""""""""""""""""""""""""""""""""""""

"""""""""""""
" LEADER KEY
"""""""""""""
let mapleader = ","

"""""""""""""
" SAVE QUIT "
"""""""""""""
nnoremap <c-s> :w<cr>
inoremap <silent> <c-s> <esc>:w<cr>a
" Make :Q and :W work like :q and :w
command! W w
command! Q q


""""""""""""""""""""""""""""""""""""""""""""""""""""
"                    NAVIGATION                    "
""""""""""""""""""""""""""""""""""""""""""""""""""""

""""""""""""""""""""
" window navigation
""""""""""""""""""""
" ctrl+hjkl move cursor to the next window + tmux panes
let g:tmux_navigator_no_mappings = 1
nnoremap <silent> <c-h> :TmuxNavigateLeft<cr>
nnoremap <silent> <c-j> :TmuxNavigateDown<cr>
nnoremap <silent> <c-k> :TmuxNavigateUp<cr>
nnoremap <silent> <c-l> :TmuxNavigateRight<cr>


""""""""""""""""""
" line navigation
""""""""""""""""""
noremap j gj
noremap k gk

"""""""""""""""""""""""""""""""""
" quick buffer and tab switching
"""""""""""""""""""""""""""""""""
nnoremap <leader>. <c-^>
nnoremap ö :bprev<cr>
nnoremap ä :bnext<cr>
nnoremap Ö :tabp<cr>
nnoremap Ä :tabn<cr>

"""""""""""""""""""""""""""""""""""""""""""""""""""""""
"                    FUNCTIONALITY                    "
"""""""""""""""""""""""""""""""""""""""""""""""""""""""

"""""""""""""""""""""""""""
" clear match highlighting
"""""""""""""""""""""""""""
noremap <leader><space> :noh<cr>:call clearmatches()<cr>

"""""""
" json
"""""""
nnoremap <leader>j :%!python3 -m json.tool<cr>

"""""""""""""""""""""""
" highlight duplicates
"""""""""""""""""""""""
nnoremap <leader>hd :syn clear Repeat \| g/^\(.*\)\n\ze\%(.*\n\)*\1$/exe 'syn match Repeat "^' . escape(getline('.'), '".\^$*[]') . '$"' \| nohlsearch<cr>


""""""""""""""""""""""""""""""""""""""""""""""""""
"                    F Tasten                    "
""""""""""""""""""""""""""""""""""""""""""""""""""
" F4 Generate Tags
nnoremap <f4> :!echo "TAGS werden generiert, dies kann einen Moment dauern..."&&ctags -R --fields=+aimlS --sort=yes -h ".h.c.cpp.sh.py.js.php.java" --languages=php,cpp,c,sh,py,js,java * 2>/dev/null<cr>


"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"                    ERRORS AND WARNINGS                    "
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" quickfix (language server?)
nnoremap <leader>p :cprev<cr>
nnoremap <leader>n :cnext<cr>
nnoremap <leader>o :copen<cr>
" location (ale?)
nnoremap <leader>P :lprev<cr>
nnoremap <leader>N :lnext<cr>
nnoremap <leader>O :lopen<cr>


"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"                    Plugin Key Bindings                    "
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

""""""""
" CtrlP
""""""""
let g:ctrlp_map = '<c-p>'

"""""""""""
" NerdTree
"""""""""""
map <C-n> :NERDTreeToggle<cr>

"""""""""""""""""""
" Markdown Preview
"""""""""""""""""""
let vim_markdown_preview_hotkey='<C-m>'
nnoremap <leader>rm :RMarkdown html<cr>

""""""""""""""
" Neosnippets
""""""""""""""
imap <C-k>     <Plug>(neosnippet_expand_or_jump)
smap <C-k>     <Plug>(neosnippet_expand_or_jump)
xmap <C-k>     <Plug>(neosnippet_expand_target)


"""""""""""""""""""""""""""""""""""""""""""""
"                    IDE                    "
"""""""""""""""""""""""""""""""""""""""""""""

"""""""
" ALE "
"""""""
function SetALEShortcuts()
    nnoremap <leader>af :ALEFix<cr>
    nnoremap <leader>as :ALEFixSuggest<cr>
endfunction()

function SetALELSPShortcuts()
    nnoremap <c-]> :ALEGoToDefinition<cr>
    nnoremap <leader>r :ALEFindReferences<cr>
    nnoremap gK :ALEHover<cr>
endfunction()

""""""""""""""""""
" Language Server
""""""""""""""""""
function SetLSPShortcuts()
    " GOTO:
    nnoremap <c-]> :call LanguageClient#textDocument_definition()<cr>
    " REFACTORING:
    nnoremap <f2> :call LanguageClient#textDocument_rename()<cr>
    " FORMAT:
    nnoremap <c-f> :call LanguageClient#textDocument_formatting()<cr>
    " gq{motion} formats the code
    setl formatexpr=LanguageClient#textDocument_rangeFormatting()<cr>
    " FIX:
    nnoremap <leader><c-f> :python print("not implemented")<cr>
    " SHOW:
    " show references (usage)
    nnoremap <leader>u :call LanguageClient#textDocument_references()<cr>
    " show implementations (interface)
    nnoremap <leader>i :python print("not implemented")
    " K shows manual entry to the element
    " shows definition
    nnoremap gK :call LanguageClient#textDocument_hover()<cr>
    " show highlighted
    nnoremap <leader>hh :call LanguageClient#textDocument_documentHighlight()<cr>
    " clear highlighted elements
    nnoremap <leader>h<space> :call LanguageClient#clearDocumentHighlight()<cr>
    " NEW:
    " available other options
    nnoremap <Leader>a :call LanguageClient#textDocument_codeAction()<cr>
endfunction()

"""""""""""""""""""""""""
" Language Server C Sharp
"""""""""""""""""""""""""
function SetCSShortcuts()
    " GOTO:
    nnoremap <c-]> :OmniSharpGotoDefinition<cr>
    " REFACTORING:
    nnoremap <f2> :OmniSharpRename<cr>
    " FORMAT:
    nnoremap <c-f> :OmniSharpCodeFormat<cr>
    " FIX:
    nnoremap <leader><c-f> :OmniSharpFixUsings<CR>
    " SHOW:
    " show references (usage)
    nnoremap <leader>u :OmniSharpFindUsages<cr>
    " show implementations (interface)
    nnoremap <leader>i :OmniSharpFindImplementations<cr>
    " K shows manual entry to the element
    " shows definition
    nnoremap gK :OmniSharpDocumentation<cr>
    " show highlighted
    nnoremap <leader>hh :python print("not implemented")<cr>
    " clear highlighted elements
    nnoremap <leader>h<space> :python print("not implemented")<cr>
    " NEW:
    nnoremap <Leader>a :OmniSharpGetCodeActions<CR>
endfunction()

""""""""""""""""""
" PHP Refactoring
""""""""""""""""""
function SetPHPShortcuts()
    nnoremap <leader>rlv :call PhpRenameLocalVariable()<cr>
    nnoremap <leader>rcv :call PhpRenameClassVariable()<cr>
    nnoremap <leader>rrm :call PhpRenameMethod()<cr>
    nnoremap <leader>reu :call PhpExtractUse()<cr>
    vnoremap <leader>rec :call PhpExtractConst()<cr>
    nnoremap <leader>rep :call PhpExtractClassProperty()<cr>
    vnoremap <leader>rem :call PhpExtractMethod()<cr>
    nnoremap <leader>rnp :call PhpCreateProperty()<cr>
    nnoremap <leader>rdu :call PhpDetectUnusedUseStatements()<cr>
    vnoremap <leader>r== :call PhpAlignAssigns()<cr>
    nnoremap <leader>rsg :call PhpCreateSettersAndGetters()<cr>
    " Not sure if i need this
    " set completefunc=LanguageClient#complete
    " set formatexpr=LanguageClient_textDocument_rangeFormatting()
endfunction()

function SetVueShortcuts()
    " FORMAT:
    nnoremap <c-f> :!yarn prettier --write %<cr>
endfunction()

"""""""""""""""""""""
" LANGUAGE SETTINGS "
"""""""""""""""""""""
" ALE:
call SetALEShortcuts()
" LSP:
autocmd FileType cpp,c,java,python,rust,fortran,typescript,javascript,javascript.jsx,dart call SetLSPShortcuts()
autocmd FileType vue call SetVueShortcuts()
autocmd FileType cs call SetCSShortcuts()

" autocmd FileType call SetALELSPShortcuts()
" Only PHP:
autocmd FileType php call SetPHPShortcuts()
