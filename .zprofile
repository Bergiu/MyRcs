###################################################
# Used for executing commands at start
# will be sourced when starting as a login shell
#
# wird einmal nach dem einloggen geladen
#
# also used to set environment variables in arch,
# because the /etc/profile is loaded after zshenv
# file
###################################################
# load my environment vars
source ~/.profile
echo "login shell"

