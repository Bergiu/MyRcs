## install dependencies

### arch install
```
sudo pacman -S --needed python3 python-pip zsh gvim neovim emacs tmux i3 dmenu base-devel git wget pygtk playerctl arandr octave feh redshift pcmanfm mpv youtube-dl mps-youtube htop tree mlocate netcat lxappearance acpi tor openssl ca-certificates termite gnome-alsamixer xorg-xbacklight networkmanager networkmanager-openvpn nm-connection-editor qt5-base qt5-tools compton inotify-tools xsel xclip w3m ranger ffmpegthumbnailer fzf networkmanager-dispatcher-ntpd
```

## pip dependencies
```
sudo pip3 install neovim neovim-remote grip i3ipc dbus-python pygobject
```

## clone repo
```
git clone --recursive https://github.com/bergiu/myrcs
cd myrcs
mkdir -p ../.config/i3
mv .config/i3/* ../.config/i3
mv .config/* ../.config/
mkdir -p ../.local/share/nvim/site/
mv .* ..
mv * ..
cd ..
rmdir myrcs
```

## git config
### configfile
```
cp .gitconfig_copy .gitconfig
```
change gpgsign to false in .gitconfig

### generate keys
TODO

## zsh install
```
sh -c "$(curl -fsSL https://raw.githubusercontent.com/robbyrussell/oh-my-zsh/master/tools/install.sh)"
git clone https://github.com/zsh-users/zsh-autosuggestions $ZSH_CUSTOM/plugins/zsh-autosuggestions
git clone https://github.com/zsh-users/zsh-syntax-highlighting $ZSH_CUSTOM/plugins/zsh-syntax-highlighting
mv .zshrc.pre-oh-my-zsh .zshrc
```

## change default shell
```
chsh -s /usr/bin/zsh
```

## scripts folder
### link scripts
Link the scripts from `~/.scripte` to an directory from the PATH for example `/usr/local/bin`.
- jdtls

### install scripts
- open\_with\_linux.py
	- `cd .scripte && ./open_with_linux.py install`

## flameshot installation
### arch
```
sudo pacman -S flameshot libappindicator-gtk3
```

## mpris-scrobbler installieren
- [github.com/mariusor/mpris-scrobbler](https://github.com/mariusor/mpris-scrobbler)
- [Bug](https://github.com/mariusor/mpris-scrobbler/issues/48)

### rest
```
mkdir -p ~/Downloads/Programme
cd ~/Downloads/Programme
git clone https://github.com/lupoDharkael/flameshot
cd flameshot
qmake && make
sudo make install
cd ~
mkdir Bilder
```

## vim/nvim update
```
source .aliases
vimupdate
nvimupdate
```

## other programs
install one of them:
- icedove, thunderbird
- iceweasel, firefox

maybe install them too:
- xf86-video-intel
- chromium, apache
- mumble, telegram, weechat
	- disable desktop notifications and sound
- eclipse, eclim
- owncloud, nextcloud
- noto-fonts, noto-fonts-emoji, otf-fira-code, cantarell-fonts

## activate tor
- tor status needs systemd
TODO

## themes
configure your theme, for example black mate
- xfce4-appearance-settings
- lxappearance

## vpn:
- copy files to /opt
- config vpn in networkmanager

## Rest
- copy backups to the right places
- login to firefox sync


# Neovim config
## Add a new language:
All config files are located in `.config/nvim/`.

- Language Server:
    1. In `04_plugin_config.vim`:
        1. Add a [languageserver](https://langserver.org/) to `g:LanguageClient_serverCommands`
        2. Disable this language server from `b:ale_linters`
            - You see all linters with the command `:ALEFixSuggest`
    2. At the bottom of `05_keybindings.vim` add the language to the `autocmd` that calls `SetLSPShortcuts`
    3. Notice in `02_plugin_install.vim` which language server you have installed
- Fixers and Linters (`04_plugin_config.vim`):
    - Install [linters](https://github.com/w0rp/ale/blob/master/supported-tools.md) and add them to `b:ale_linters`
    - Install [fixers](https://github.com/w0rp/ale/blob/master/supported-tools.md) and add them to `b:ale_fixers`
- Use ALE als Language Client
    - Add LanguageServer to `b:ale_linters`
    - Add the language to the `autocmd` that calls `SetALELSPShortcuts`
    - Remove the language from the `autocmd` that calls `SetLSPShortcuts`

## Install:
- Shell: shellcheck, shfmt
    - `pacman -S shellcheck shfmt`
- Python:
    - `pip install vulture rope pyflakes mccabe pycodestyle pydocstyle autopep8 yapf mypy isort python-language-server pyls-mypy pyls-isort`

## Keybindings
- FORMAT
    - `gq{motion}` Format code
    - `={motion}` Indent code
    - `<c-f>` Format full code (LSP)
- GOTO
    - `<c-]>` Jump into tag
    - `<c-]>` Jump into definition (LSP/ALE)
    - `<c-o>` Go back
- SHOW
    - `,s` Show symbols
    - `,r` Show references
    - `K` Shows manual entry of element
    - `gK` Show definition (LSP/ALE)
    - `,hd` Highlight duplicates
    - `,hh` Highlight (LSP)
    - `,h<space>` Clean lsp highlight (LSP)
    - `,<space>` Clean search highlights
- REFACTORING
    - `<f2>` Rename (LSP)
- ALE
    - `,af` AleFix
    - `,as` AleFixSuggest


## Firefox darktheme fix
- `cd ~/.mozilla/firefox/*.default`
- `mkdir chrome`
- `cp ~/.config/mozilla/firefox/chrome/userContent.css ~/.mozilla/firefox/*.default/chrome/`


# Ruby Installation
- `apt install ruby rbenv ruby-contrib`
- `apt purge ruby-build`
- `mkdir -p "$(rbenv root)"/plugins`
- `git clone https://github.com/rbenv/ruby-build.git "$(rbenv root)"/plugins/ruby-build`

## Update Ruby Build
- https://github.com/rbenv/ruby-build
- `git -C "$(rbenv root)"/plugins/ruby-build pull`
