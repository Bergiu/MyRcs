Mathe Zeichen:
∞ u+221e infinity
± u+b1 plus minus
≤ u+2264 kleiner gleich
≥ u+2265 größer gleich
≠ u+02260 ungleich/ not equal
≈ u+2248 ungefähr gleich
÷ u+f7 division
× u+d7 multiplication
· u+b7 dot
• u+2022 big dot
∈ u+2208 element of
∉ u+2209 not an element of
ℝ u+211d  Reelle Zahlen
∅ u+2205 leere menge
∥ u+2225 parallel
⊥ u+22a5 senkrecht
∧ u+2227 logical and
∨ u+2228 logical or
∫ u+222b integral
∬ u+222c doppel integral
∭ u+222d tripple integral
√ u+221a root
∩ u+2229 vereinigung
∪ u+222a geschnitten

Mathe Buchstaben:
Δ u+0394 delta
λ u+03bb small lambda
π u+03c0 small pi
α u+03b1 small alpha
β u+03b2 small beta
γ u+03b3 small gamma

Superscript:
⁰ u+2070 superscript 0
¹ u+b9 superscript 1
² u+b2 superscript 2
³ u+b3 superscript 3
⁴ u+2074 superscript 4
...
⁹ u+2079 superscript 9
⁺ u+207a superscript +
⁻ u+207b superscript -
⁼ u+207c superscript =
⁽ u+207d superscript (
⁾ u+207e superscript )
ⁿ u+207f superscript n
ⁱ u+2071 superscript i

Subscript:

Other math unicodes:
http://xahlee.info/comp/unicode_math_operators.html

Random:
జ్ఞ‌ా U+0C1C U+0C4D U+0C1E U+200C U+0C3E Telugu Char that kills iPhones
স্র‌ু U+09B8 U+09CD U+09B0 U+200C U+09C1 Bengali Chat that kills iPhones
Ͳ u+0372 big archaic sampi
Ϟ u+03de small koppa
卐 u+5350 swastika1
卍 u+534d swastika2
߷ u+07f7 fidget spinner
﷽ u+fdfd basmala (In the name of God, the Most Gracious, the Most Merciful)

Bidirectional characters:
‏ u+200f rtl zeichen (LRM) (schiebt den text auf die rechte seite)
‎ u+200e ltr zeichen (RLM)
؜ u+061c arabic letter mark ALM) (schiebt den text auf die rechte seite)
‪ u+202a ltr embedding (LRE)
‭ u+202d ltr override (LRO)
‫ u+202b rtl embedding (RLE)
‮ u+202e rtl override (RLO) (dreht den text um, also aus abc wird cba)
‬ u+202c pop directional formatting (PDF)
⁦u+2066 ltr isolate (LRI)
⁧u+2067 rtl isolate (RLI)
⁨u+2068 first strong isolate (FSI)
⁩u+2069 pop directional isolate (PDI)

Blank characters:
⠀ u+2800 braille pattern blank
​ u+200b zero width space
﻿ u+feff zero-width non-breaking space
https://en.wikipedia.org/wiki/Unicode_character_property#Whitespace
