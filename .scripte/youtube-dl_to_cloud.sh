#!/bin/bash
notify-send DownloadToCloud "Download Started!"
ssh server-poweredge-global "/scripts/youtube-dl_to_cloud.sh \"$1\""
notify-send DownloadToCloud "Download Finished!"
