## open\_with\_linux.py
- Needs to be installed with `./open_with_linux.py install`
- Requirement of https://addons.mozilla.org/en-US/firefox/addon/open-with/


## Vim Debug:
.vimspector.json Example:
```
{
  "configurations": {
    "<name>: Launch": {
      "adapter": "debugpy",
      "configuration": {
        "name": "<name>: Launch",
        "type": "python",
        "request": "launch",
        "cwd": "~/.scripte",
        "python": "/usr/bin/python3",
        "stopOnEntry": true,
        "console": "externalTerminal",
        "debugOptions": [],
        "program": "add_to_kodi.py",
        "args": ["--help"]
      }
    }
  }
}
```

