#!/usr/bin/env python3

import argparse
import requests

from enum import Enum
from typing import Union, Tuple, List, Optional, Any
from dataclasses import dataclass

from kodijsonrpc import KodiJSONClient
from youtube_dl import YoutubeDL
from youtube_dl.utils import DownloadError


class LinkType(Enum):
    Unknown = 0
    Video = 1
    Audio = 2


@dataclass
class Link:
    url: str
    link_type: LinkType


@dataclass
class Format:
    format: Any
    link_type: LinkType

    def get_link(self) -> Link:
        return Link(self.format["url"], self.link_type)


def unescape(raw_url: str) -> str:
    """Remove backslashes.

    This is sometimes needed when the script is called for example from firefox which
    will escape all special characters, but that will break youtube_dl.

    Args:
        raw_url: An escaped url.

    Returns:
        str: The unescaped url.
    """
    # XXX: only remove single ones and not doubles
    return raw_url.replace("\\", "")
    # maybe this? if yes, remove this function
    # return urllib.parse.unquote(url)


def _get_highest_video_quality_format(formats):
    format_prio = ["720p", "480p", "360p", "240p", "144p"]
    for priorized in format_prio:
        filtered = list(filter(lambda form: form["format_note"] == priorized, formats))
        if len(filtered) > 0:
            return filtered[0]
    return None


def _get_highest_audio_quality_format(formats):
    highest = 0
    highest_item = None
    for form in formats:
        if form["abr"] > highest:
            highest = form["abr"]
            highest_item = form
    return highest_item


def get_highest_video_quality_format(formats) -> Format:
    """Searches for highest quality, but only until 720p."""
    both_formats = list(filter(lambda form: form["acodec"] != "none" and form["vcodec"] != "none", formats))
    audio_formats = list(filter(lambda form: form["acodec"] != "none", formats))
    video_formats = list(filter(lambda form: form["vcodec"] != "none", formats))
    if len(both_formats) > 0:
        form = _get_highest_video_quality_format(both_formats)
        if form is not None:
            format = Format(form, LinkType.Video)
            return format
    if len(audio_formats) > 0:
        form = _get_highest_audio_quality_format(audio_formats)
        if form is not None:
            return Format(form, LinkType.Audio)
    if len(video_formats) > 0:
        form = _get_highest_video_quality_format(video_formats)
        if form is not None:
            return Format(form, LinkType.Video)
    return Format(formats[0], LinkType.Unknown)


def extract_video_url(raw_url: str) -> Union[List[Link], Link, None]:
    """Extract the video url from a video embedded in a website.

    Uses youtube_dl to get the video url of a video that is embedded into a website.

    Args:
        raw_url: The url of the website.

    Returns:
        str: The video url.
    """
    download_url = ""
    youtube_dl = YoutubeDL(params={"ignoreerrors": True})
    try:
        infos = youtube_dl.extract_info(raw_url, download=False)
    except:
        return None
    if "extractor" in infos.keys():
        if infos["extractor"] == "youtube:tab":
            print("Youtube Video")
            if "entries" in infos.keys():
                download_urls = []
                for entry in infos["entries"]:
                    if entry is None:
                        continue
                    if "formats" in entry.keys():
                        format = get_highest_video_quality_format(entry["formats"])
                        download_urls.append(format.get_link())
                if len(download_urls) > 0:
                    return download_urls
    if "url" in infos.keys():
        download_url = infos["url"]
        return [Link(download_url, LinkType.Unknown)]
    if "formats" in infos.keys():
        # get an id
        # TODO: option to select the format
        format = get_highest_video_quality_format(infos["formats"])
        return [format.get_link()]
    return None


def get_link_list(raw_url: str) -> List[Link]:
    """Try to get a video url from a website.

    First uses youtube_dl to extract the url. If that fails it tries again with an
    unescaped url.

    Args:
        raw_url: The url of the website.

    Returns:
        str: The url of the video.
    """
    urls = extract_video_url(raw_url)
    if urls is None:
        urls = extract_video_url(unescape(raw_url))
        if urls is None:
            urls = [Link(raw_url, LinkType.Unknown)]
    return urls


def login(url: str, port: str, username: str, password: str) -> KodiJSONClient:
    """Login to Kodi.

    Login to a kodi instance and tests the connection with a ping.

    Args:
        url: Url to the kodi instance.
        port: Port of the kodi web interface.
        username: Kodi username.
        password: Kodi password.

    Returns:
        KodiJSONClient: A kodi client.
    """
    server = KodiJSONClient(url, port, username, password)
    server.JSONRPC.Ping()
    return server


def _add_single(server: KodiJSONClient, link: Link):
    url = link.url
    link_type = link.link_type
    if link_type == LinkType.Audio:
        server.Playlist.Add(playlistid=0, item={"file": url})
    elif link_type == LinkType.Video:
        server.Playlist.Add(playlistid=1, item={"file": url})
    elif link_type == LinkType.Unknown:
        server.Playlist.Add(playlistid=0, item={"file": url})
        server.Playlist.Add(playlistid=1, item={"file": url})
    else:
        raise Exception(f"Unknown LinkType: {link_type}")


def add(server: KodiJSONClient, links: List[Link]) -> None:
    """Add a url in kodi to the playlist.

    Sends the specified video url to kodi and kodi plays the video.

    Args:
        server: The kodi json client that is already connected to the kodi instance.
        url: Url to add. Needs to be a url that contains a video/audio and no html.
    """
    for link in links:
        _add_single(server, link)


def add_youtube_dl(url: str, kodi_url: str, port: str, username: str,
                    password: str) -> None:
    """Add a video in kodi to playlist.

    Fetches the url from the video embedded in the given url with youtube_dl.
    Then it sends the video url to kodi and plays it.

    Args:
        raw_url: The original url from the website containing the video.
    """
    links = get_link_list(url)
    if len(links) < 3:
        print("Found urls:")
        print(links)
    else:
        print("Found many urls.")
    print("Login to Kodi.")
    try:
        kodi_ctrl = login(kodi_url, port, username, password)
    except requests.exceptions.ConnectionError as error:
        print(str(error))
        print("Maybe wrong login credentials.")
        return
    print("Sending to Kodi.")
    add(kodi_ctrl, links)
    print("Finished.")


def parse_args():
    """Parse arguments."""
    parser = argparse.ArgumentParser(
        description="Extracts video urls and streams them to kodi.")
    required_named = parser.add_argument_group('required named arguments')
    required_named.add_argument(
        "--kodi-url",
        help="url of the kodi instance",
        action="store",
        required=True)
    parser.add_argument(
        "--kodi-port",
        nargs="?",
        help="port of the kodi web interface",
        action="store",
        default="8080")
    parser.add_argument(
        "--username",
        nargs="?",
        help="kodi username",
        action="store",
        default=None)
    parser.add_argument(
        "--password",
        nargs="?",
        help="kodi password",
        action="store",
        default=None)
    parser.add_argument(
        "url", help="url of the website with the video", action="store")
    return parser.parse_args()


def main():
    args = parse_args()
    add_youtube_dl(
        args.url,
        args.kodi_url,
        port=args.kodi_port,
        username=args.username,
        password=args.password)


main()
