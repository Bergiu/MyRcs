function printhelp {
	echo "Usage: script [OPTION]

Options:
  -v, --verbose		show verbose output
  -h, --help		display this help and exit
  -nc, --no-color	no color

Output:
  status_remote status_repo status_commit path_to_repo

Output explanation:
  status_remote = 0 => ok
  status_remote = 1 => could not read from remote
  status_remote = 2 => no username or password
  status_remote = 3 => authentication failed
  status_remote = 5 => unknown

  status_repo = 0 => up to date
  status_repo = 1 => ahead
  status_repo = 2 => behind
  status_repo = 3 => ahead and behind
  status_repo = 4 => empty repo
  status_repo = 5 => unknown

  status_commit = 0 => nothing
  status_commit = 1 => changes
"
}

function printverbose {
	[[ $verbose -ge 1 ]] && echo $1 >&2
}

verbose=0
colorenable=1
if [[ -n $1 ]]; then
	if [[ $1 == "-h" ]]; then
		printhelp
		exit
	fi
	if [[ $1 == "--help" ]]; then
		printhelp
		exit
	fi
	if [[ $1 == "-v" ]] || [[ $1 == "--verbose" ]] || [[ $2 == "-v" ]] || [[ $2 == "--verbose" ]]; then
		verbose=1
	fi
	if [[ $1 == "-nc" ]] || [[ $1 == "--no-color" ]] || [[ $2 == "-nc" ]] || [[ $2 == "--no-color" ]]; then
		colorenable=0
	fi
fi

cw=$PWD;
# find ~ -name ".*" -prune -a -name ".git" | while read x; do
while read x; do
	echo "$x/.." >&2
	printverbose "- cd to $x/.."
	cd "$x/.."
	[[ ! $? -eq 0 ]] && printverbose "- no folder with this name" && continue
	printverbose "- get remote update"
	# status_remote = 0 => ok
	# status_remote = 1 => could not read from remote
	# status_remote = 2 => no username or password
	# status_remote = 3 => authentication failed
	# status_remote = 4 => unknown
	status_remote=0
	s_rem=$(git remote update 2>&1)
	if [[ ! $? -eq 0 ]]; then
		tmp=$(echo $s_rem | grep "fatal: Could not read from remote repository.")
		if [[ $? -eq 0 ]]; then
			status_remote=1
		else
			tmp=$(echo $s_rem | grep "fatal: could not read Username")
			if [[ $? -eq 0 ]]; then
				status_remote=2
				echo ""
			else
				tmp=$(echo $s_rem | grep "fatal: could not read Password")
				if [[ $? -eq 0 ]]; then
					status_remote=2
				else
					tmp=$(echo $s_rem | grep "fatal: Authentication failed for")
					if [[ $? -eq 0 ]]; then
						status_remote=3
					else
						status_remote=4
					fi
				fi
			fi
		fi
	fi
	printverbose "- get status"
	# status_repo = 0 => up to date
	# status_repo = 1 => ahead
	# status_repo = 2 => behind
	# status_repo = 3 => ahead and behind
	# status_repo = 4 => empty repo
	# status_repo = 5 => unknown
	stattxt=$(git status -uno)
	tmp==$(echo $stattxt | grep "Your branch is up to date with")
	if [[ $? -eq 0 ]]; then # 0 := match
		status_repo=0
	else
		tmp==$(echo $stattxt | grep "Your branch is behind")
		if [[ $? -eq 0 ]]; then
			status_repo=1
		else
			tmp==$(echo $stattxt | grep "Your branch is ahead of")
			if [[ $? -eq 0 ]]; then
				status_repo=2
			else
				tmp==$(echo $stattxt | grep -E "Your branch and .* have diverged")
				if [[ $? -eq 0 ]]; then
					status_repo=3
				else
					tmp1=$(echo $stattxt | grep "On branch master")
					tmp1stat=$?
					tmp2=$(echo $stattxt | grep "No commits yet")
					tmp2stat=$?
					tmp3=$(echo $stattxt | grep "nothing to commit")
					tmp3stat=$?
					if [[ $tmp1stat -eq 0 ]] && [[ $tmp2stat -eq 0 ]] && [[ $tmp3stat -eq 0 ]]; then
						status_repo=4
					else
						status_repo=5
					fi
				fi
			fi
		fi
	fi
	# status_commit = 0 => nothing
	# status_commit = 1 => changes
	stattxt=$(git status)
	tmp==$(echo $stattxt | grep -i "nothing to commit")
	if [[ $? -eq 0 ]]; then
		status_commit=0
	else
		status_commit=1
	fi
	nc=""
	bg_color=""
	if [[ $colorenable -eq 1 ]]; then
		IGreen="\033[0;92m"
		IYellow="\033[0;93m"
		IRed="\033[0;91m"
		IBlue="\033[0;94m"
		nc="\033[0m"
		sum=$(( $status_remote + $status_repo + $status_commit ))
		if [[ $sum -eq 0 ]]; then
			# if everything is ok
			bg_color=$IGreen
		else
			if [[ $status_remote -gt 0 ]] || [[ $status_repo -eq 3 ]] || [[ $status_repo -eq 5 ]]; then
				# if remote is broken, or merge is incoming
				bg_color=$IRed
			else
				if [[ ! $status_commit -gt 0 ]]; then
					bg_color=$IYellow
				else
					# if nothing is wrong but there are changes
					gb_color=$IBlue
				fi
			fi
		fi
	fi
	[[ ! -n $out ]] && out="${bg_color}$status_remote $status_repo $status_commit${nc} $x/.." || out="$out\n${bg_color}$status_remote $status_repo $status_commit${nc} $x/.."
	cd "$cw";
done <<< "$(find ~ -type d -name ".*" -prune -a -name ".git")"
cd "$cw"
echo -e $out
