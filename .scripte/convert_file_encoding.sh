#!/bin/bash
#loop to convert multiple files
for file in *.txt; do
	#enter input encoding here
	FROM_ENCODING=`file -i $file | cut -d "=" -f2`
	#output encoding(UTF-8)
	TO_ENCODING="UTF-8"
	#convert
	if [[ ! $FROM_ENCODING = "utf-8" ]]; then
		echo "non utf8"
	fi
	CONVERT=" iconv -f $FROM_ENCODING -t $TO_ENCODING"
	$CONVERT "$file" -o "${file%.txt}.txt"
done
exit 0
