#!/usr/bin/env python3

import argparse
import requests

from typing import Union

from kodijsonrpc import KodiJSONClient
from youtube_dl import YoutubeDL
from youtube_dl.utils import DownloadError


def unescape(raw_url: str) -> str:
    """Remove backslashes.

    This is sometimes needed when the script is called for example from firefox which
    will escape all special characters, but that will break youtube_dl.

    Args:
        raw_url: An escaped url.

    Returns:
        str: The unescaped url.
    """
    # XXX: only remove single ones and not doubles
    return raw_url.replace("\\", "")
    # maybe this? if yes, remove this function
    # return urllib.parse.unquote(url)


def _get_highest_video_quality_format(formats):
    format_prio = ["720p", "480p", "360p", "240p", "144p"]
    for priorized in format_prio:
        filtered = list(filter(lambda form: form["format_note"] == priorized, formats))
        if len(filtered) > 0:
            return filtered[0]
    return None


def _get_highest_audio_quality_format(formats):
    highest = 0
    highest_item = None
    for form in formats:
        if form["abr"] > highest:
            highest = form["abr"]
            highest_item = form
    return highest_item


def get_highest_video_quality_format(formats):
    """Searches for highest quality, but only until 720p. If there is no video + audio
    link it has a fallback to only audio. if there is also no audio link, it will
    use the only video links."""
    both_formats = list(filter(lambda form: form["acodec"] != "none" and form["vcodec"] != "none", formats))
    audio_formats = list(filter(lambda form: form["acodec"] != "none", formats))
    video_formats = list(filter(lambda form: form["vcodec"] != "none", formats))
    solved = False
    if len(both_formats) > 0:
        form = _get_highest_video_quality_format(both_formats)
        if form is not None:
            solved = True
    if len(audio_formats) > 0 and not solved:
        form = _get_highest_audio_quality_format(audio_formats)
        if form is not None:
            solved = True
    if len(video_formats) > 0 and not solved:
        form = _get_highest_video_quality_format(video_formats)
        if form is not None:
            solved = True
    if not solved:
        return formats[0]
    else:
        return form


def extract_video_url(raw_url: str) -> Union[str, None]:
    """Extract the video url from a video embedded in a website.

    Uses youtube_dl to get the video url of a video that is embedded into a website.

    Args:
        raw_url: The url of the website.

    Returns:
        str: The video url.
    """
    download_url = ""
    youtube_dl = YoutubeDL()
    try:
        infos = youtube_dl.extract_info(raw_url, download=False)
    except:
        return None
    if "url" in infos.keys():
        download_url = infos["url"]
    elif "formats" in infos.keys():
        # get an id
        # TODO: option to select the format
        form = get_highest_video_quality_format(infos["formats"])
        download_url = form["url"]
    return download_url


def get_url(raw_url: str) -> str:
    """Try to get a video url from a website.

    First uses youtube_dl to extract the url. If that fails it tries again with an
    unescaped url.

    Args:
        raw_url: The url of the website.

    Returns:
        str: The url of the video.
    """
    download_url = extract_video_url(raw_url)
    if download_url is None:
        download_url = extract_video_url(unescape(raw_url))
    return download_url


def login(url: str, port: str, username: str, password: str) -> KodiJSONClient:
    """Login to Kodi.

    Login to a kodi instance and tests the connection with a ping.

    Args:
        url: Url to the kodi instance.
        port: Port of the kodi web interface.
        username: Kodi username.
        password: Kodi password.

    Returns:
        KodiJSONClient: A kodi client.
    """
    server = KodiJSONClient(url, port, username, password)
    server.JSONRPC.Ping()
    return server


def play(server: KodiJSONClient, url: str) -> None:
    """Play a url in kodi.

    Sends the specified video url to kodi and kodi plays the video.

    Args:
        server: The kodi json client that is already connected to the kodi instance.
        url: Url to play. Needs to be a url that contains a video and no html.
    """
    server.Player.Open(item={"file": url})


def play_youtube_dl(url: str, kodi_url: str, port: str, username: str,
                    password: str) -> None:
    """Play a video in kodi.

    Fetches the url from the video embedded in the given url with youtube_dl.
    Then it sends the video url to kodi and plays it.

    Args:
        raw_url: The original url from the website containing the video.
    """
    download_url = get_url(url)
    if download_url is None:
        download_url = url
    print("Found url:")
    print(download_url)
    print("Login to Kodi.")
    try:
        kodi_ctrl = login(kodi_url, port, username, password)
    except requests.exceptions.ConnectionError as error:
        print(str(error))
        print("Maybe wrong login credentials.")
        return
    print("Sending to Kodi.")
    play(kodi_ctrl, download_url)
    print("Finished.")


def parse_args():
    """Parse arguments."""
    parser = argparse.ArgumentParser(
        description="Extracts video urls and streams them to kodi.")
    required_named = parser.add_argument_group('required named arguments')
    required_named.add_argument(
        "--kodi-url",
        help="url of the kodi instance",
        action="store",
        required=True)
    parser.add_argument(
        "--kodi-port",
        nargs="?",
        help="port of the kodi web interface",
        action="store",
        default="8080")
    parser.add_argument(
        "--username",
        nargs="?",
        help="kodi username",
        action="store",
        default=None)
    parser.add_argument(
        "--password",
        nargs="?",
        help="kodi password",
        action="store",
        default=None)
    parser.add_argument(
        "url", help="url of the website with the video", action="store")
    return parser.parse_args()


def main():
    args = parse_args()
    play_youtube_dl(
        args.url,
        args.kodi_url,
        port=args.kodi_port,
        username=args.username,
        password=args.password)


main()
