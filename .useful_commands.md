## useful commands
command                                          | help
-------------------------------------------------|--------
yaourt -Syu                                      | Update all Packages
chsh                                             | change default shell
mc                                               | opens midnight commander
mkdir -p a/b                                     | erstellt beide ordner aufeinmal
df -h                                            | show disk space
du -sh ./\*                                      | show size of all folders (ohne \\)
setxkdbmap de                                    | deutsches tastaturlayout
sed s/alt/neu datei                              | suchen und ersetzen
xargs -I {} ls {}/                               | use {} as the param
sudo chattr +i /etc/resolv.conf                  | make the file unchangeable
python -m json.tool                              | normalize json
echo "ab" \| sudo tee file                       | schreibt ab mit root rechten in die datei file
who                                              |
last                                             |
dmesg                                            |
apci -V                                          | see battery
dirname file                                     |
basename file                                    |
flpsed                                           |
ldconfig --print-cache                           |
fc-cache -f -v                                   | updates font cache
vlock                                            | lock terminal
strace                                           | trace systemcalls
bear make                                        | build a compile\_commands.json file from make command

## X
command                                          | help
-------------------------------------------------|--------
xrandr                                           | display settings terminal
arandr                                           | display settings gui
xprop                                            | get the class of a program

# media
command                                          | help
-------------------------------------------------|--------
nativefier                                       | convert website to desktop app
scrot                                            | make screenshot
scrot -s                                         | make screenshot
input                                            | make screenshot
ttystudio                                        | record terminal as gif
mpv -vo tct video.mp4                            | play videos inside of the terminal
mpv -vo caca video.mp4                           | play videos inside of the terminal
tcv bild.jpg                                     | show pictures inside of the termianl https://github.com/whentze/tcv

# useful programs
command                                          | help
-------------------------------------------------|--------
qalc                                             | calculator
octave                                           | mathe program
irssi                                            | irc client (terminal)
ncTelegram                                       | telegram client (terminal)
latexmk                                          | auto compile latex
sloccount                                        | counts lines of code
factor                                           | factorizes a number

# network
command                                          | help
-------------------------------------------------|--------
netstat -tulpen                                  | zeigt netzwerkdienste an (depreciated)
ss                                               | zeigt netzwerdienste an (kp wie das funktioniert)
nmap -A ip                                       | zeigt offene ports von der ip an
nmap -A -p 0-65535 ip                            | zeigt offene ports von der ip an
whois google.de                                  | zeigt den whois eintrag der seite an

## fun
command                                          | help
-------------------------------------------------|--------
archey3                                          | screenfetch
neofetch                                         | screenfetch
cowsay                                           | cows
ponysay                                          | ponys
aafire                                           | fire
sl                                               | train
tac                                              | cat inverse
lolcat                                           | colorizes text
toilet -f mono9 --termwidth                      | displays messages
curl http://a.4cdn.org/g/thread/60791595.json    | browse 4chans json api
vrms                                             | shows unfree programs
cat /dev/urandom \| base64                       | hacking

## other useful stuff
```bash
cd `dirname $0`
dirname=$(dirname file)
base=$(basename $file)
name=${base%.*};
ext=${base##*.};
```

## useful manpages
```
man file
man test
man tr
man seq
man sh
man sort
man uniq
man wc
man awk
man sed
man pgrep
man pkill
man cut
man find
man xargs
man head
man tail
man install
man tee
man join
man fold
```

## freunde
strace, gdb, ftrace, dtrace, ktrace, valgrind, xtrace, perf

`[ test kram hier ] && irgendein befehl || else befehl`

## press keys from terminal
DISPLAY=:0 xdotool keydown ctrl+Tab
DISPLAY=:0 xdotool keyup ctrl+Tab

## switch audio to bluetooth
bluetoothctl
> pair MM:AA:CC:AD:RE:SS:EE
> connect MM:AA:CC:AD:RE:SS:EE
pavucontrol

## switch audio output
pactl list short sinks | show audio devices
pactl set-default-sink alsa\_output.pci-0000\_00\_03.0.hdmi-surround-extra1

## change display brightness
light -A 30 # add 30
light -U 30 # substract 30

## sed
- -i datei
	- ändere datei
- s/../../g
	- mehr als einmal pro zeile

## change keyboard layout
[link](https://superuser.com/questions/646425/permanently-change-default-language-and-keyboard-settings-what-am-i-missing)

## change screen resolution
- `xrandr --newmode "1920x1080\_60.00"  172.80  1920 2040 2248 2576  1080 1081 1084 1118  -HSync +Vsync`
- `xrandr --addmode Virtual1 1920x1080\_60.00`
- `xrandr --output Virtual1 --mode 1920x1080\_60.00`

## Oh My Zsh
`sh -c "$(curl -fsSL https://raw.githubusercontent.com/robbyrussell/oh-my-zsh/master/tools/install.sh)"`

## dependencies (i3, and general)
- i3
- i3lock
- dmenu
- pulseaudio
- pulseaudio-equalizer (für pactl)
- amixer
- playerctl
- [fontawesome](https://github.com/FortAwesome/Font-Awesome/releases)
	- [cheatsheet](http://fontawesome.io/cheatsheet/)
	- mkdir ~/.fonts
	- mv fonts/\*.ttf ~/.fonts
- scrot
- shutter
- nm-applet
- telegram
- lxappearance
- gtk-theme-arc
	1. open lxappearance
	2. select adwaita theme dark (looks cooler than arc but arc is nessesary for firefox)
- arc dark theme firefox
	- add the following line to ~/.mozilla/firefox/...default/chrome/userChrome.css:
		#personal-bookmarks .bookmark-item > .toolbarbutton-text {color:#d3dae3!important}
- thunar file explorer
- compton ( transparency an effects )
- acpi
- system san francisco font
- python2
- pygtk
- qalc
- xbacklight
- nmcli
- egrep, awk

## Update
- apt update / pacman ...
- vim
	- PluginInstall
	- PluginUpdate
	- PluginClean
- tmux
	- ctrl-a I
	- ctrl-a U

## Python
```
from pudb import set_trace; set_trace()
```
Adds a python debugger on this line
